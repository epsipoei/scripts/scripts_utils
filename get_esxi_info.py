from pyVim.connect import SmartConnect, Disconnect
from pyVmomi import vim
import ssl

def connect_to_esxi(host, user, password):
    """
    Connect to ESXi host using provided credentials.
    """
    try:
        # Disable SSL certificate verification
        context = ssl._create_unverified_context()
        
        service_instance = SmartConnect(host=host, user=user, pwd=password, sslContext=context)
        if not service_instance:
            raise Exception("Failed to connect to ESXi host.")
        return service_instance
    except Exception as e:
        print(f"Error connecting to ESXi host: {str(e)}")
        return None


def disconnect_from_esxi(service_instance):
    """
    Disconnect from ESXi host.
    """
    if service_instance:
        Disconnect(service_instance)

def get_esxi_info(service_instance):
    """
    Retrieve ESXi information such as datacenter, cluster, resource pool, network, datastore, and host ID.
    """
    content = service_instance.RetrieveContent()

    # Get datacenter
    datacenter = content.rootFolder.childEntity[0].name

    # Get cluster
    cluster = content.rootFolder.childEntity[0].hostFolder.childEntity[0].name

    # Get resource pool
    pool = content.rootFolder.childEntity[0].hostFolder.childEntity[0].resourcePool._moId

    # Get network
    network = content.rootFolder.childEntity[0].network[0].name

    # Get datastore if available
    datastores = content.rootFolder.childEntity[0].datastore
    if datastores:
        datastore = datastores[0].info.name
        datastore_id = datastores[0].info.url
    else:
        datastore = "No datastore available"
        datastore_id = ""

    # Get host ID
    host = content.rootFolder.childEntity[0].hostFolder.childEntity[0].host[0].name
    host_id = content.rootFolder.childEntity[0].hostFolder.childEntity[0].host[0]._moId

    return {
        "Nom_de_votre_datacenter": datacenter,
        "Nom_de_votre_cluster": cluster,
        "Nom_de_votre_pool_de_ressources": pool,
        "Nom_de_votre_reseau": network,
        "Nom_de_votre_datastore": datastore,
        "ID_de_votre_datastore": datastore_id,
        "ID_de_votre_hote_ESXi": host_id
    }


if __name__ == "__main__":
    # Modify these variables with your ESXi host credentials
    HOST = ""
    USER = ""
    PASSWORD = ""

    # Connect to ESXi host
    service_instance = connect_to_esxi(HOST, USER, PASSWORD)
    if service_instance:
        try:
            # Get ESXi information
            esxi_info = get_esxi_info(service_instance)
            print("ESXi Information:")
            for key, value in esxi_info.items():
                print(f"{key}: {value}")
        finally:
            # Disconnect from ESXi host
            disconnect_from_esxi(service_instance)
