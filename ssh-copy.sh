#!/bin/bash

# Définition des variables
TARGET_HOST="adresse_ip_ou_nom_d_hote"
TARGET_USER="debian"

# Copie de la clé publique vers la machine cible
ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}

